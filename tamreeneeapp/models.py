# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
# from users.models import profile



class video_model(models.Model):
	video_url = models.CharField(max_length=100,blank=True,null=True)
	video_details = models.TextField(null=True,blank=True)
	is_active = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
	updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)
	def __str__(self):
		return '%s' % (self.video_url)


class basic_user_seen_status(models.Model):
	video = models.ForeignKey(video_model)
	profile_ob = models.ForeignKey('users.profile')
	def __str__(self):
		return '%s - %s' % (self.profile_ob,self.video.video_url)


class user_calendar(models.Model):
	user = models.ForeignKey('users.profile')
	start_date = models.DateTimeField(null=True,blank=True)
	end_date = models.DateTimeField(null=True,blank=True)
	def __str__(self):
		return '%s' % (self.user)


class tips(models.Model):
	tip_name = models.CharField(max_length=100)
	tip_description = models.TextField(blank=True,null=True)
	def __str__(self):
		return '%s' %(self.tip_name)








