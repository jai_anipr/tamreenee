# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from users.models import profile
from tamreenee.settings import *


from django.shortcuts import render
# Create your views here.
from .models import *
from .serializers import *
from rest_framework import viewsets,status,response
from django.contrib.auth.models import User,Group
from django.db.models import Q
import django_filters
from django.template.loader import get_template
from django.core.mail import  EmailMultiAlternatives
from rest_framework import filters
import json, requests, ast


class seen_video_status(viewsets.ViewSet):
	def create(self,request):
		try:
			video_ob = video_model.objects.get(id=request.data['video_id'])
			user_ob = profile.objects.get(id=request.data['user_id'])
			seen_ob = basic_user_seen_status.objects.create(video=video_ob,profile_ob=user_ob)
			return  response.Response({"success":"Basic User seen the video"},status=status.HTTP_200_OK)
		except Exception as e:
			return  response.Response({"error":e.message},status=status.HTTP_400_BAD_REQUEST)

class service_video_viewset(viewsets.ModelViewSet):
	queryset = video_model.objects.all().order_by('-updated_at')
	serializer_class = service_video_serializer
	# permission_classes = ('Isuser, Isadmin')

	def create(self, request, *args, **kwargs):
		try:
			video_ob  = video_model.objects.create(video_url = request.data['video_url'])
			if 'video_details' in request.data:
				video_ob.video_details = request.data['video_details']
			if 'is_active' in request.data:
				video_ob.is_active = request.data['is_active']
			video_ob.save()
			return response.Response({"success": "Video created successfully"}, status=status.HTTP_200_OK)
		except Exception as e:
			return response.Response({"error": e.message}, status=status.HTTP_400_BAD_REQUEST)

	def update(self, request, *args, **kwargs):
		try:
			video_ob = self.get_object()
			if 'video_url' in request.data:
				video_ob.video_url = request.data['video_url']
			if 'video_details' in request.data:
				video_ob.video_details = request.data['video_details']
			if 'is_active' in request.data:
				video_ob.is_active = request.data['is_active']
			video_ob.save()
			return response.Response({"success": "Video details updated"}, status=status.HTTP_200_OK)
		except Exception as e:
			return response.Response({"error": e.message}, status=status.HTTP_400_BAD_REQUEST)



class get_all_the_videos(viewsets.ViewSet):
	def list(self,request):
		try:
			all_user_videos = video_model.objects.all()
			serializer = service_video_serializer(instance=all_user_videos,many=True)
			for i in serializer.data:
				a = seen_video_status.objects.get(video= i['id'],profile_ob= request.GET.get('user_id'))
				if a:
					i['seen_status'] = True
				else:
					i['seen_status'] = False
			return response.Response(serializer.data,status=status.HTTP_200_OK)
		except Exception as e:
			return response.Response({"error":e.message},status=status.HTTP_400_BAD_REQUEST)

class get_workboard_library(viewsets.ViewSet):
	def create(self,request):
		pass
	def list(self,request):
		try:
			pass
		except Exception as e:
			pass


def send_email_notification(request, data):
	content = {

        "mobile": data['mobile'],
        "name": data['name']
    }
	pwdtemplate = get_template('signup.html')
	htmlt = pwdtemplate.render(content)
	to = ["jaisimha15@gmail.com"]
	by = "no-reply@tamreenee.ae"
	subject = "Welcome Mail Tamreenee"
	bcc = []
	msg = EmailMultiAlternatives(subject, "", by, to, bcc)
	msg.attach_alternative(htmlt, "text/html")
	msg.send()

class tips_viewset(viewsets.ModelViewSet):
	queryset = tips.objects.all()
	serializer_class = tips_serializer

	def create(self, request, *args, **kwargs):
		try:
			tip_ob = tips(tip_name = request.data['tip_name'])
			if 'tip_description' in request.data:
				tip_ob.tip_description = request.data['tip_description']
			tip_ob.save()
			return  response.Response('tip created successfully',status=status.HTTP_200_OK)
		except Exception as e:
			return response.Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)
	def update(self, request, *args, **kwargs):
		try:
			tip_ob = self.get_object()
			if 'tip_name' in request.data:
				tip_ob.tip_name = request.data['tip_name']
			if 'tip_description' in request.data:
				tip_ob.tip_description = request.data['tip_description']
			return response.Response('tip updated successfully',status=status.HTTP_200_OK)
		except Exception as e:
			return response.Response({"error":e.message},status=status.HTTP_400_BAD_REQUEST)
