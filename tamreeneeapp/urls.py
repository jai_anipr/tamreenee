from django.conf.urls import include, url
from django.contrib import admin

from rest_framework.routers import DefaultRouter,SimpleRouter

from .views import *

router = SimpleRouter()

router.register(r'videos',service_video_viewset, 'videos')
# router.register(r'dashboard_count',dashboard_count, 'dashboard_count')
router.register(r'seen_video_status',seen_video_status, 'seen_video_status')
router.register(r'get_all_the_videos',get_all_the_videos, 'get_all_the_videos')
router.register(r'tips',tips_viewset, 'tips')
urlpatterns = router.urls
