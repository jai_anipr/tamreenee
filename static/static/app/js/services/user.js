TamreeneeAPP.factory('Users', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getUsers = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createUser = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getUserInfo = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteUser = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.updateUserInfo = function(id,data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end users service here 
TamreeneeAPP.factory('Login', function($q, $http){
  var object = {};
	object.Login = function(data){
	var defered = $q.defer();
	$http({
	  url : '/api/v1/login',
	  method: 'POST',
	  data: data
	}).then(function(success){
	  defered.resolve(success.data);
	},function(error){
	 	 defered.reject(error.data);
	});
		return defered.promise;
	}
  object.resetPassword = function(data){
    var defered = $q.defer();
    $http({
      url : '/api/v1/forgot-password-process',
      method : 'POST',
      data : data
    }).then(function(success){
      defered.resolve(success.data);
    },
      function(error){
        defered.reject(error.data);       
      }
    );
    return defered.promise;
  } 

  object.forgotPassword = function(data){
    var defered = $q.defer();
    $http({
      url : '/api/v1/forgot-password',
      method : 'POST',
      data : data
    }).then(function(success){
      defered.resolve(success.data);
    },
      function(error){
        defered.reject(error.data);       
      }
    );
    return defered.promise;
  }
  object.changePassword = function(data){
    var defered = $q.defer();
    $http({
      url : '/api/v1/change_password',
      method : 'POST',
      data : data
    }).then(function(success){
      defered.resolve(success.data);
    },
      function(error){
        defered.reject(error.data);       
      }
    );
    return defered.promise;
  }
  return object;
});
// Logout service
TamreeneeAPP.factory('Logout', function($q, $http){
  var object = {};
  object.Logoutcustomer = function(){
    var defered = $q.defer();
    $http({
      url : '/api/v1/logout/',
      method: 'GET',
    }).then(function(success){
      defered.resolve(success.data);
    },
    function(error){
      defered.reject(error.data);
    });
    return defered.promise;
  }
  return object;
});
// Logout service ends