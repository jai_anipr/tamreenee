TamreeneeAPP.factory('ServiceVideos', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getServiceVideos = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/service_videos/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createServiceVideo = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/service_videos/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getServiceVideoInfo = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/service_videos/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteServiceVideo = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/service_videos/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.updateServiceVideoInfo = function(id,data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/service_videos/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);