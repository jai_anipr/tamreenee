var TamreeneeAPP = angular.module('TamreeneeApp', ['ui.router','oc.lazyLoad']);

TamreeneeAPP.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider

        .state('home', {
          url: "/home", 
          templateUrl: 'static/app/views/home.html',
          controller: 'HomeCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/app/js/controllers/home.js');
            }]
          }
        })
        
});

TamreeneeAPP.controller('TamreeneeCtrl', function($scope) {
    
    $scope.message = 'Index page calling';
    
});