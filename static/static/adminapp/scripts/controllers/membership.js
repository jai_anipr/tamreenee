angular.module("TamreeneeAdminApp").controller('MembershipCtrl', function($scope,$auth,$state,Memberships) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    Memberships.getMemberships().then(function(success){
      //console.log("success", success)
      $scope.memberships = success
      jQuery( document ).ready( function( $ ) {
            setTimeout(function(){ 
                var $table = jQuery( '#table' );
                $table.DataTable( {
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "bStateSave": true
                });
            }, 100);
        } );
    }, function(error){
      //console.log("error", error)
    })
    $scope.DeleteMembership = function(obj){
      //console.log("obj.id", obj)
      swal({
        title: "Are you sure?",
        text: "You Want to Delete it!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
        Memberships.deleteMembership(obj.id).then(function(success){
          swal("Deleted!", "Deleted Successfully!.", "success");
          $state.go("membership",{},{reload: "membership"})
        }, function(error){

        })
      });
    }
});
angular.module("TamreeneeAdminApp").controller('CreateMembershipCtrl', function($scope,$auth,$state,Memberships) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    $scope.AddMembership = function(){
      Memberships.createMembership($scope.membership).then(function(success){
        swal("Good job !", "Membership Created Successfully !", "success")
        $state.go('membership')
      }, function(error){
        swal("Error !", "Something Went Wrong !", "error")

      })
    }
});