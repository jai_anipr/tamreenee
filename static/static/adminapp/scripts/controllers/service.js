angular.module("TamreeneeAdminApp").controller('ServiceCtrl', function($scope,$auth,$state,Services) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    Services.getServices().then(function(success){
      $scope.services = success

        jQuery( document ).ready( function( $ ) {
            setTimeout(function(){ 
                var $table = jQuery( '#table' );
                $table.DataTable( {
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "bStateSave": true
                });
            }, 100);
        } );

    }, function(error){
    })

    $scope.ViewService = function(obj){
      localStorage.setItem('ServiceId', obj.id)
      $state.go("individual_service")
    }
    $scope.DeleteService = function(obj){
      swal({
        title: "Are you sure?",
        text: "You Want to Delete it!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
        Services.deleteService(obj.id).then(function(success){
          swal("Deleted!", "Deleted Successfully!.", "success");
          $state.go("service",{},{reload: "service"})
        }, function(error){

        })
      });
    }

});
angular.module("TamreeneeAdminApp").controller('individualServiceCtrl', function($scope,$auth,$state,Services) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    var ServiceId = localStorage.getItem('ServiceId')
    if(ServiceId){
      Services.getServiceInfo(ServiceId).then(function(success){
        $scope.service = success
      }, function(error){
      })
    }else{
      $state.go('service')
    }
    
    
});
angular.module("TamreeneeAdminApp").filter("trustUrl", ['$sce',
  function($sce) {
    return function(recordingUrl) {
      return $sce.trustAsResourceUrl(recordingUrl);
    };
  }
]);
angular.module("TamreeneeAdminApp").controller('CreateServiceCtrl', function($scope,$auth,$state, Services) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    $('.showvideo').hide();
    $scope.servicevideos = []
    document.getElementById("upload_widget_opener").addEventListener("click", function() {
      cloudinary.openUploadWidget({ 
          cloud_name: 'jaicloudinary', 
          upload_preset: 'xkjcix7q'
      }, function(error, result) { 
          document.getElementById('videonumber').innerHTML = result.length
          angular.forEach(result, function(individualvideo){
            $scope.servicevideos.push({'video_url': individualvideo.url})
          })
          $('.showvideo').show();
      });
    }, false);
    $scope.AddService = function(){
      $scope.service = {
        'service_name' : $scope.service_name,
        'service_details' : $scope.service_details,
        'service_videos' : $scope.servicevideos
      }
      Services.createService($scope.service).then(function(success){
        swal("Good job !", "Service Created Successfully !", "success")
        $state.go('service')
      }, function(error){
        swal("Error !", "Something Went Wrong !", "error")

      })
    }

});