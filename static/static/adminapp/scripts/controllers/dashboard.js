angular.module("TamreeneeAdminApp").controller('dashboardCtrl', function($scope, $http,$auth,$state) {
    //console.log("admin home page calling")
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    $http({
	    url : '/api/v1/dashboard_count/',
	    method : 'GET'
  	}).then(function(success){
  		//console.log("success", success)
  		$scope.dashboardcount = success.data
  	},function(error){
	    //console.log("error", error)
	});
});