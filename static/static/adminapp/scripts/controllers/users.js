angular.module("TamreeneeAdminApp").controller('UsersCtrl', function($scope, Users,$state,$auth) {
    //console.log("Users page calling")
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    Users.getUsers().then(function(success){
    	//console.log("success", success)
    	$scope.users = success

        jQuery( document ).ready( function( $ ) {
            setTimeout(function(){ 
                var $table = jQuery( '#table' );
                $table.DataTable( {
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "bStateSave": true
                });
            }, 100);
        } );


    }, function(error){
    	//console.log("error", error)
    })
});