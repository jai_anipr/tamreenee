angular.module("TamreeneeAdminApp").controller('AdminloginCtrl', function($scope,$state, AdminLogin,$auth) {
    //console.log("admin login page calling")
    $scope.Login = function(){
    	// $state.go('dashboard')
    	//console.log("user", $scope.user)
    	AdminLogin.Login($scope.user).then(function(success){
    		//console.log("success", success)
    		localStorage.setItem('Tamreeneeuserdata', JSON.stringify(success));
    		$state.go('dashboard')
    		$auth.setToken(success.token)
    		location.reload();
    	}, function (error){
    		//console.log("error", error)
    		swal({
			  title: "Error !",
			  text: "incorrect email or password !",
			  icon: "error",
			  button: "Close",
			});
    	})
    }
    if($auth.isAuthenticated()){
      $state.go('dashboard')
    }
});