angular.module("TamreeneeAdminApp").controller('PlanCtrl', function($scope,$auth,$state,Plans) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    Plans.getPlans().then(function(success){
      //console.log("success", success)
      $scope.plans = success
      jQuery( document ).ready( function( $ ) {
            setTimeout(function(){ 
                var $table = jQuery( '#table' );
                $table.DataTable( {
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "bStateSave": true
                });
            }, 100);
        } );
    }, function(error){
      //console.log("error", error)
    })
    $scope.DeletePlan = function(obj){
      //console.log("obj.id", obj)
      swal({
        title: "Are you sure?",
        text: "You Want to Delete it!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
        Plans.deletePlan(obj.id).then(function(success){
          swal("Deleted!", "Deleted Successfully!.", "success");
          $state.go("plan",{},{reload: "plan"})
        }, function(error){

        })
      });
    }
});
angular.module("TamreeneeAdminApp").controller('CreatePlanCtrl', function($scope,$auth,$state,Plans) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    $scope.AddLevel = function(){
      Plans.createPlan($scope.plan).then(function(success){
        swal("Good job !", "Plan Created Successfully !", "success")
        $state.go('plan')
      }, function(error){
        swal("Error !", "Something Went Wrong !", "error")

      })
    }
});