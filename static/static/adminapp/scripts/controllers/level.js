angular.module("TamreeneeAdminApp").controller('LevelCtrl', function($scope,$auth,$state,Levels) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    Levels.getLevels().then(function(success){
      //console.log("success", success)
      $scope.levels = success
      jQuery( document ).ready( function( $ ) {
            setTimeout(function(){ 
                var $table = jQuery( '#table' );
                $table.DataTable( {
                    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "bStateSave": true
                });
            }, 100);
        } );
    }, function(error){
      //console.log("error", error)
    })
    $scope.DeleteLevel = function(obj){
      swal({
        title: "Are you sure?",
        text: "You Want to Delete it!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){
        Levels.deleteLevel(obj.id).then(function(success){
          swal("Deleted!", "Deleted Successfully!.", "success");
          $state.go("level",{},{reload: "level"})
        }, function(error){

        })
      });
    }
});
angular.module("TamreeneeAdminApp").controller('CreateLevelCtrl', function($scope,$auth,$state,Levels) {
    if(!$auth.isAuthenticated()){
      $state.go('adminlogin')
    }
    $scope.AddLevel = function(){
      Levels.createLevel($scope.level).then(function(success){
        swal("Good job !", "Level Created Successfully !", "success")
        $state.go('level')
      }, function(error){
        swal("Error !", "Something Went Wrong !", "error")

      })
    }
});