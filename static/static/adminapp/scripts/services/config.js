TamreeneeAdminAPP.factory('Levels', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getLevels = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/levels/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createLevel = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/levels/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getLevelInfo = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/levels/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteLevel = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/levels/'+id,
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.updateLevelInfo = function(id,data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/levels/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);

TamreeneeAdminAPP.factory('Plans', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getPlans = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/plans/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createPlan = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/plans/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getPlanInfo = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/plans/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deletePlan = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/plans/'+id,
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.updatePlanInfo = function(id,data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/plans/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);

TamreeneeAdminAPP.factory('Memberships', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getMemberships = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/membership/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createMembership = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/membership/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getMembershipInfo = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/membership/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteMembership = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/membership/'+id,
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.updateMembershipInfo = function(id,data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/membership/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);