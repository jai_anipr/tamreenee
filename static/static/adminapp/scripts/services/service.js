TamreeneeAdminAPP.factory('Services', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getServices = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/services/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createService = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/services/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getServiceInfo = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/services/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteService = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/services/'+id,
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.updateServiceInfo = function(id,data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/services/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);