TamreeneeAdminAPP.factory('Users', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getUsers = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createUser = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getUserInfo = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteUser = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.updateUserInfo = function(id,data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end users service here 
TamreeneeAdminAPP.factory('AdminLogin', function($q, $http){
  var object = {};
  object.Login = function(data){
    var defered = $q.defer();
    $http({
      url : '/api/v1/adminlogin/',
      method: 'POST',
      data: data
    }).then(function(success){
      defered.resolve(success.data);
    },function(error){
       defered.reject(error.data);
    });
      return defered.promise;
  }
  return object;
});