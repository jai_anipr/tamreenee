var TamreeneeAdminAPP = angular.module('TamreeneeAdminApp', ['ui.router','oc.lazyLoad','satellizer']);

TamreeneeAdminAPP.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/adminlogin');
    
    $stateProvider

        .state('dashboard', {
          url: "/dashboard", 
          templateUrl: 'static/adminapp/views/dashboard.html',
          controller: 'dashboardCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/dashboard.js');
            }]
          }
        })
        .state('adminlogin', {
          url: "/adminlogin", 
          templateUrl: 'static/adminapp/views/login.html',
          controller: 'AdminloginCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/login.js');
            }]
          }
        })
        .state('users', {
          url: "/users", 
          templateUrl: 'static/adminapp/views/users.html',
          controller: 'UsersCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/users.js');
            }]
          }
        })
        .state('service', {
          url: "/service", 
          templateUrl: 'static/adminapp/views/service.html',
          controller: 'ServiceCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/service.js');
            }]
          }
        })
        .state('individual_service', {
          url: "/individual_service", 
          templateUrl: 'static/adminapp/views/individual_service.html',
          controller: 'individualServiceCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/service.js');
            }]
          }
        })
        .state('create_service', {
          url: "/create_service", 
          templateUrl: 'static/adminapp/views/create_service.html',
          controller: 'CreateServiceCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/service.js');
            }]
          }
        })
        .state('level', {
          url: "/level", 
          templateUrl: 'static/adminapp/views/level.html',
          controller: 'LevelCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/level.js');
            }]
          }
        })
        .state('create_level', {
          url: "/create_level", 
          templateUrl: 'static/adminapp/views/create_level.html',
          controller: 'CreateLevelCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/level.js');
            }]
          }
        })
        .state('plan', {
          url: "/plan", 
          templateUrl: 'static/adminapp/views/plan.html',
          controller: 'PlanCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/plan.js');
            }]
          }
        })
        .state('create_plan', {
          url: "/create_plan", 
          templateUrl: 'static/adminapp/views/create_plan.html',
          controller: 'CreatePlanCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/plan.js');
            }]
          }
        })
        .state('membership', {
          url: "/membership", 
          templateUrl: 'static/adminapp/views/membership.html',
          controller: 'MembershipCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/membership.js');
            }]
          }
        })
        .state('create_membership', {
          url: "/create_membership", 
          templateUrl: 'static/adminapp/views/create_membership.html',
          controller: 'CreateMembershipCtrl',
          resolve: { 
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/static/adminapp/scripts/controllers/membership.js');
            }]
          }
        })
        
});

TamreeneeAdminAPP.controller('adminHomeCtrl', function($scope,$auth,$window,$state) {

    $scope.admindata = JSON.parse(localStorage.getItem('Tamreeneeuserdata'));
    //console.log("$scope.Admindata", $scope.admindata)
    $scope.Logout = function(){
      //console.log("Logout");
      $state.go('adminlogin');
      $auth.removeToken()
      $auth.logout()
      $window.localStorage.clear();
      // $localStorage.$reset();
    }
});