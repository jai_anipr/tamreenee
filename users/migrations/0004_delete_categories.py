# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-02-27 06:53
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_remove_profile_profile_category'),
    ]

    operations = [
        migrations.DeleteModel(
            name='categories',
        ),
    ]
