from rest_framework.serializers import ModelSerializer
from .models import *

class user_serializer(ModelSerializer):

    class Meta:
        model = profile
        fields = '__all__'


from django.contrib.auth.models import Group
class group_serializer(ModelSerializer):
    class Meta:
        model = Group
        fields = '__all__'

class djan_user_serializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username','email','first_name','last_name')

class work_level_serializer(ModelSerializer):
    class Meta:
        model = work_levels
        fields = ('id','level_name','level_description',)

class membership_serializer(ModelSerializer):
    class Meta:
        model = membership
        fields = ('id','membership_name', 'membership_description',)

# class plan_serializer(ModelSerializer):
#     class Meta:
#         model = plans
#         fields = ('id','plan_name', 'plan_description','plan_price',)

class profile_serializer(ModelSerializer):
    user = djan_user_serializer()
    profile_level = work_level_serializer()
    profile_membership = membership_serializer()
    class Meta:
        model = profile
        fields = '__all__'



class followserializer(ModelSerializer):
    class Meta:
        model = Follower
        fields = '__all__'

