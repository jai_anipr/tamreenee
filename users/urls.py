from .views import *
from rest_framework.routers import format_suffix_patterns, url
from rest_framework.routers import SimpleRouter
from rest_framework_jwt.views import verify_jwt_token
from django.contrib import admin

login_view_set = LoginViewset.as_view({
	'post': 'create'
})
logout_view_set = LogoutViewset.as_view({
	'get': 'list'})


router = SimpleRouter()

router.register(r'users', pbuser_viewset, 'users')
router.register(r'groups', group_viewset, 'groups')
router.register(r'adminlogin', Admin_login, 'adminlogin')
router.register(r'change_password', change_password, 'change_password')
# router.register(r'plans', plan_viewset, 'plans')
router.register(r'levels', work_level_viewset, 'levels')
router.register(r'membership', membership_viewset, 'membership')

router.register(r'forgot-password', ForgotPasswordViewset, 'forgot-password')
router.register(r'forgot-password-process', ForgotPasswordProcessViewset, 'forgot-password-process')
router.register(r'facebook_login', FacebookLoginViewset, 'facebook_login')

router.register(r'follow',follow_viewset,'follow')
router.register(r'unfollow',unfollow , 'unfollow')
router.register(r'my_followers',my_followers , 'my_followers')
router.register(r'me_following',me_following , 'me_following')


urlpatterns = router.urls

urlpatterns += format_suffix_patterns([
    url(r'^login$',login_view_set, name='login-view-set'),
    url(r'^logout$',logout_view_set, name='logout-view-set'),])
