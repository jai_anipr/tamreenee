# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session

# Create your models here.
# from tamreeneeapp.models import services

class work_levels(models.Model):
    level_name =  models.CharField(max_length=100)
    level_description = models.TextField(null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    def __str__(self):
        return '%s' % (self.level_name)

class membership(models.Model):
    membership_name =  models.CharField(max_length=150)
    membership_description = models.TextField(null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    def __str__(self):
        return '%s' % (self.membership_name)


# class plans(models.Model):
#     plan_name =  models.CharField(max_length=100)
#     plan_description = models.TextField(null=True,blank=True)
#     plan_price = models.IntegerField(null=True,blank=True)
#     created = models.DateTimeField(auto_now_add=True)
#     updated = models.DateTimeField(auto_now=True)
#     def __str__(self):
#         return '%s' % (self.plan_name)


class profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gender = models.CharField(max_length=50, blank=True, null=True)
    mobile = models.CharField(max_length=50, null=True, blank=True)
    mobile2 = models.CharField(max_length=50, null=True, blank=True)
    activate_token = models.CharField(null=False, default="", max_length=500, blank=True)
    forgot_pass_token = models.CharField(null=False, default="", max_length=500, blank=True)
    dob = models.CharField(null=False, default="", max_length=50, blank=True)
    address = models.CharField(null=False, default="", max_length=500, blank=True)
    height =  models.CharField(blank=True,null=True,max_length=10)
    motivation_quote = models.CharField(blank=True,null=True,max_length=500)
    weight = models.CharField(blank=True,null=True,max_length=50)
    city = models.CharField(null=False, default="", max_length=500, blank=True)
    state = models.CharField(null=False, default="", max_length=500, blank=True)
    cover_image = models.CharField(null=False, default="", max_length=500, blank=True)
    device_token = models.CharField(null=False,max_length=500, blank=True)
    # profile_plan = models.ForeignKey(plans, null=True,blank=True)
    profile_membership = models.ForeignKey(membership, null=True, blank=True)
    profile_level = models.ForeignKey(work_levels, null=True, blank=True)
    # profile_services = models.ManyToManyField(services,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.user)

class UserSessions(models.Model):
    user = models.ForeignKey(User, related_name='user_sessions')
    session = models.ForeignKey(Session, related_name='user_sessions',
                                on_delete=models.CASCADE)
    def __str__(self):
        return '%s - %s' % (self.user, self.session.session_key)



class Follower(models.Model):
    follower = models.ForeignKey(profile, related_name='following')
    following = models.ForeignKey(profile, related_name='followers')

    def __str__(self):
        return '%s follows %s' % (self.follower, self.following)