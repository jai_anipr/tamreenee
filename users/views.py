# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from tamreenee.settings import *
from django.shortcuts import render
# Create your views here.
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from django.contrib.auth.models import User
from serializers import *
from .models import *
from django.contrib.sessions.models import Session
from rest_framework.decorators import detail_route
from django.contrib.auth import authenticate, login, logout
from rest_framework import status, viewsets
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token
from django.conf import settings
# imports from settings.py
from tamreenee.settings import *
from rest_framework import response,status
# Permission classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import BasicAuthentication

from django.contrib.auth.signals import user_logged_out
from rest_framework_jwt.settings import api_settings
from datetime import datetime, timedelta
import jwt
import json, requests, ast
import datetime

#permissions and authentication
from rest_framework.authentication import BasicAuthentication, SessionAuthentication, TokenAuthentication
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

#mail sending plugins
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string

import django_filters
from  rest_framework import filters

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

import urllib
import json

def create_token(userid):
	payload = {
	'sub': str(userid),
		'iat': datetime.datetime.now(),
		'exp': datetime.datetime.now() + timedelta(days=3000)
	}
	token = jwt.encode(payload, SECRET_KEY)
	return token.decode('unicode_escape')


class LoginViewset(ViewSet):
	def create(self, request):
		# city, state = GetCurrentLocation(request)
		# request_ip = get_client_ip(request)

		# checking input put field are equal to database fields
		allowed_fields = ['email', 'password']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		mandatory_temp = set(allowed_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in allowed_fields)
			return Response({'error': 'mandatory fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:
			user = authenticate(username=request.data['email'], password=request.data['password'])
			user_default_obj = User.objects.get(username=request.data['email'])
			if user is not None and user_default_obj.is_active:
				login(request, user)
				if request.user.is_authenticated():
					session = Session.objects.filter(session_key=request.session.session_key)
					if session:
						user_session = UserSessions.objects.create(user=request.user, session=session[0])
				else:
					return Response({"message": "Invalid password. please try again"}, status=400)

				payload = jwt_payload_handler(user)
				profile_ob = profile.objects.get(user_id=user.id)
				if api_settings.JWT_ALLOW_REFRESH:
					payload['orig_iat'] = timegm(
						datetime.utcnow().utctimetuple()
					)

				token = jwt_encode_handler(payload)
				user_data = {}
				user_data['first_name'] = user.first_name
				user_data['last_name'] = user.last_name
				user_data['mobile'] = profile_ob.mobile
				user_data['cover_image'] = profile_ob.cover_image
				#user_data['user_id'] = user.id
				user_data['email'] = user.email

				request.session['auth_user_id'] = user.id
				request.session['profile_id'] = profile_ob.id
				print ("request session", request.session)

				return Response({'token': token,
								 'user': [user_data],
								 'id': profile_ob.id,
								 'groups': user.groups.all().values_list('name', flat=True),

								 'message': 'User successfully logged in'}, status=200)

			# return Response({"message":"User logged in successfully"})
			else:
				return response.Response({"message":"Invalid Credentials"},status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print ("error", e)
			return Response({"message": "Invalid Credentials"}, status=400)


class all_login(ViewSet):
	def create(self, request):
		# city, state = GetCurrentLocation(request)
		# request_ip = get_client_ip(request)

		# checking input put field are equal to database fields
		allowed_fields = ['email', 'password']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		mandatory_temp = set(allowed_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in allowed_fields)
			return Response({'error': 'mandatory fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:
			user = authenticate(username=request.data['email'], password=request.data['password'])
			user_default_obj = User.objects.get(username=request.data['email'])
			if user is not None and user_default_obj.is_active:
				login(request, user)
				if request.user.is_authenticated():
					session = Session.objects.filter(session_key=request.session.session_key)
					if session:
						user_session = UserSessions.objects.create(user=request.user, session=session[0])
				else:
					return Response({"message": "Invalid password. please try again"}, status=400)

				payload = jwt_payload_handler(user)

				profile_ob = profile.objects.get(user_id=user.id)
				if api_settings.JWT_ALLOW_REFRESH:
					payload['orig_iat'] = timegm(
						datetime.utcnow().utctimetuple()
					)

				token = jwt_encode_handler(payload)
				user_data = {}
				user_data['first_name'] = user.first_name
				user_data['last_name'] = user.last_name
				user_data['mobile'] = profile_ob.mobile
				user_data['cover_image'] = profile_ob.cover_image
				user_data['user_id'] = user.id
				user_data['email'] = user.email

				request.session['auth_user_id'] = user.id
				request.session['profile_id'] = profile_ob.id
				print ("request session", request.session)

				return Response({'token': token,
								 'user': [user_data],
								 'id': profile_ob.id,
								 'groups': user.groups.all().values_list('name', flat=True),

								 'message': 'User successfully logged in'}, status=200)

			else:
				return Response({"message":"Incorrect password"},status=status.HTTP_400_BAD_REQUEST)
			# return Response({"message":"User logged in successfully"})
		except Exception as e:
			print ("error", e)
			return Response({"message":"Invalid Credentials"}, status=400)



class Admin_login(ViewSet):
	def create(self, request):
		# city, state = GetCurrentLocation(request)
		# request_ip = get_client_ip(request)

		# checking input put field are equal to database fields
		allowed_fields = ['email', 'password']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		mandatory_temp = set(allowed_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in allowed_fields)
			return Response({'error': 'mandatory fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:
			user = authenticate(username=request.data['email'], password=request.data['password'])
			user_default_obj = User.objects.get(username=request.data['email'])
			if user is not None and user_default_obj.is_active:
				login(request, user)
				if request.user.is_authenticated():
					session = Session.objects.filter(session_key=request.session.session_key)
					if session:
						user_session = UserSessions.objects.create(user=request.user, session=session[0])
				else:
					return Response({"message": "Invalid password. please try again"}, status=400)

				payload = jwt_payload_handler(user)
				a = user.groups.filter(name='appAdmin').count()
				if a == 1:
					profile_ob = profile.objects.get(user_id=user.id)
					if api_settings.JWT_ALLOW_REFRESH:
						payload['orig_iat'] = timegm(
							datetime.utcnow().utctimetuple()
						)

					token = jwt_encode_handler(payload)
					user_data = {}
					user_data['first_name'] = user.first_name
					user_data['last_name'] = user.last_name
					user_data['mobile'] = profile_ob.mobile
					user_data['cover_image'] = profile_ob.cover_image
					#user_data['user_id'] = user.id
					user_data['email'] = user.email

					request.session['auth_user_id'] = user.id
					request.session['profile_id'] = profile_ob.id
					print ("request session", request.session)

					return Response({'token': token,
									 'user': [user_data],
									 'id': profile_ob.id,
									 'groups': user.groups.all().values_list('name', flat=True),

									 'message': 'User successfully logged in'}, status=200)
				else:
					return response.Response({'message':'Invalid User Credentials'},status=status.HTTP_400_BAD_REQUEST)
				# return Response({"message":"User logged in successfully"})
			else:
				return response.Response({'message': 'incorrect email or password'}, status=status.HTTP_400_BAD_REQUEST)

		except Exception as e:
			print ("error", e)
			return Response({"message": "Invalid Credentials"}, status=400)

class LogoutViewset(ViewSet):
	def list(self, request):
		try:
			user = getattr(request, 'user', None)
			if hasattr(user, 'is_authenticated') and not user.is_authenticated():
				user = None
			user_logged_out.send(sender=user.__class__, request=request, user=user)

			request.session.flush()
			if hasattr(request, 'user'):
				from django.contrib.auth.models import AnonymousUser
				request.user = AnonymousUser()
				return Response({"success": "Logged out successfully !"}, status=200)
		except Exception as e:
			return Response({"error": e.message}, status=400)

class UserFilters(django_filters.FilterSet):
    date_gte = django_filters.DateFilter(name="created", lookup_expr='gt')
    date_lte = django_filters.DateFilter(name="created", lookup_expr='lt')
    class Meta:
        model = profile
        fields = [ 'date_gte','date_lte','user__groups__name','profile_membership','profile_level']


#sending mail when creating new supplier
def send_email_notification(request, data):
	content = {


		#"plan_price": plan_id['plan_price'],

		"name": data.user.first_name,
		"mobile":data.mobile
	}
	pwdtemplate = get_template('welcome_mail.html')


	htmlt = pwdtemplate.render(content)
	to = [data.user.email]
	by = "no-reply@tamreenee.ae"
	subject = "Welcome Mail Tamreenee"
	bcc = ["jaisimha15@gmail.com"]
	msg = EmailMultiAlternatives(subject, "", by, to, bcc)
	msg.attach_alternative(htmlt, "text/html")
	msg.send()

class pbuser_viewset(viewsets.ModelViewSet):
	queryset = profile.objects.all().order_by('-updated')
	serializer_class = profile_serializer
	filter_class = UserFilters
	filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)

	def update(self, request, pk):
		try:
			# checking input put field are equal to database fields
			allowed_fields = ['id','first_name','last_name','mobile','profile_level','profile_membership','profile_plan', 'group_name','deactivate',"height","weight","motivation_quote"]
	
			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
	
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)
			profile_details = profile.objects.get(id=int(pk))
			user_details = User.objects.get(id=int(profile_details.user.id))
			if 'first_name' in request.data:
				user_details.first_name = request.data['first_name']
			if 'last_name' in request.data:
				user_details.last_name = request.data['last_name']
			if 'mobile' in request.data:
				profile_details.mobile = request.data['mobile']

			if 'height' in request.data:
				profile_details.height = request.data['height']
			if 'weight' in request.data:
				profile_details.weight = request.data['weight']
			if 'motivation_quote' in request.data:
				profile_details.motivation_quote = request.data['motivation_quote']

			if 'profile_level' in request.data:
				profile_details.profile_level = work_levels.objects.get(id=request.data['profile_level'])

			if 'profile_membership' in request.data:
				profile_details.profile_membership = membership.objects.get(id=request.data['profile_membership'])
			if 'group_name' in request.data:
				c_group = Group.objects.get(name=request.data['group_name'])
				user_details.groups.clear()
				user_details.groups.add(c_group)
			if 'deactivate' in request.data:
				if request.data['deactivate'] != False:
					user_details.is_active = False
			profile_details.save()
			user_details.save()
			return Response({'response': 'successfully updated user information'}, status=status.HTTP_200_OK)
		except Exception as e:
			print (e)
			return Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)


	def create(self, request, *args, **kwargs):
		# checking input put field are equal to database fields
		allowed_fields = ['first_name', 'last_name','group_name', 'email', 'password', 'mobile', 'cover_image','username','profile_level','profile_membership','profile_plan']
		mandatory_fields = ['email', 'password']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		mandatory_temp = set(mandatory_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in mandatory_temp)
			return Response({'error': 'mandatory fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:
			user = User.objects.create_user(request.data['email'], request.data['email'], request.data['password'])
			user.is_active = True
			try:
				if 'first_name' in request.data:
					user.first_name = request.data['first_name']
				if 'last_name' in request.data:
					user.last_name = request.data['last_name']
				user.save()
				user_details = profile(user=user)
				if 'mobile' in request.data:
					user_details.mobile = request.data['mobile']
				if 'cover_image' in request.data:
					user_details.cover_image = request.data['cover_image']

				if 'profile_level' in request.data:
					user_details.profile_level = work_levels.objects.get(id=request.data['profile_level'])

				if 'profile_membership' in request.data:
					user_details.profile_membership = membership.objects.get(id=request.data['profile_membership'])

				user_details.save()
				if 'group_name' in request.data:
					c_group = Group.objects.get(name=request.data['group_name'])
					user.groups.add(c_group)
				# token = create_token(user.id)

				token = jwt.encode({
					'username': user.username,
					'iat': datetime.datetime.utcnow(),
					'nbf': datetime.datetime.utcnow() + datetime.timedelta(minutes=-5),
					'exp': datetime.datetime.utcnow() + datetime.timedelta(days=7)
				}, settings.SECRET_KEY)


				user_details.save()



				# send_email_notification(request, user_details)
				return Response({'success': {'message': 'You have been signup successfully !',
											 "id": user_details.id,"email": request.data['email']}},
								status=status.HTTP_200_OK)
			except Exception as e:
				print(e)
				return Response({'message': 'Email already exists'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response({'message': 'It seems you already have an account !'}, status=status.HTTP_400_BAD_REQUEST)

	def destroy(self,request,pk):
		try:
			profile_obj = profile.objects.get(id=int(pk))
			user_obj = User.objects.get(id=profile_obj.user.id)
			print ("pk",user_obj)
			user_obj.is_active = False
			user_obj.save()
			return Response({'success': {'message': 'successfully deleted !'}},status=status.HTTP_200_OK)
		except Exception as e:
			return Response({'message': 'It seems you already have an account !'}, status=status.HTTP_400_BAD_REQUEST)

from django.contrib.auth.models import Group
class group_viewset(viewsets.ModelViewSet):
	queryset = Group.objects.all()
	serializer_class = group_serializer


class change_password(viewsets.ViewSet):
	def create(self,request):
		try:
			sup_id = request.data['user_id']
			sup_ob = profile.objects.get(id=sup_id)
			password = request.data['newpassword']
			print password
			c_password = request.data['confirmpassword']
			print  c_password
			if password and c_password:
				if password != c_password:
					return Response({'error': 'password and confirm password did not match'},
									status=status.HTTP_400_BAD_REQUEST)
				else:
					current_user = User.objects.get(id=sup_ob.user.id)
					print current_user.id
					success = current_user.check_password(request.data['current_password'])

					if success:
						# current_user.set_password((request.data['new_password']))
						# print request.data['newpassword']
						current_user.set_password(request.data['newpassword'])
						current_user.save()
						return Response({'sucess': 'password changed'}, status=status.HTTP_200_OK)

					else:
						return Response({'error': 'old Password incorrect'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			return response.Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)


# sending activation main method
def send_activation_mail(request,data, token, mail_type):
	ctx = {
				"email": str(data['email']),
				"token": token,
				"server_url": request.get_host()
			}

	if mail_type == 'forgot_mail':
		subject = "Password Reset Request"
		pwdtemplate = get_template('forgotpassword.html')
		htmlt = render_to_string('forgotpassword.html', ctx)
	else:
		pwdtemplate = get_template('send_activation.html')
	# htmlt = pwdtemplate.render(content)
	to = [str(data['email'])]
	by = "noreply@tamareenee.ae"
	msg = EmailMultiAlternatives(subject, "", by, to)
	msg.attach_alternative(htmlt, "text/html")
	msg.send()

class ForgotPasswordViewset(ViewSet):
	def create(self,request):
		try:
			# ---- parameter verifications ----
			allowed_fields = ['email']
			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
			mandatory_temp = set(allowed_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in allowed_fields)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
			try:
				user = User.objects.get(email=request.data['email'])
			except Exception as e:
				print e.message
				return Response({'error':e.message}, status=status.HTTP_400_BAD_REQUEST)
			if not user:
				return Response({'error': 'no email found to this account ...'}, status=status.HTTP_400_BAD_REQUEST)
			#attaching details for sending in email
			obj = {'first_name': user.first_name, 'last_name': user.last_name, 'email': user.email}
			token = create_token(user.id)
			try:
				FrUser = profile.objects.get(user_id=user)
				FrUser.forgot_pass_token = token
				FrUser.save()
				send_activation_mail(request, obj, token, 'forgot_mail')
				return Response({'message': 'forgot password link successfully sent'}, status=status.HTTP_200_OK)
			except Exception as e:
				print (e.message)
				return Response({'error': 'no user account found with this email'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print (e)
			return Response({'error': 'some thing went wrong ...'}, status=status.HTTP_400_BAD_REQUEST)

class ForgotPasswordProcessViewset(ViewSet):
	def create(self,request):
		try:
			# ---- parameter verifications ----
			allowed_fields = ['email', 'token', 'password']
			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(allowed_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in allowed_fields)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
			try:
				user = User.objects.get(email=request.data['email'])
			except Exception as e:
				return Response({'error': 'no account found with this email'}, status=status.HTTP_400_BAD_REQUEST)
			try:
				FrUser = profile.objects.get(forgot_pass_token=request.data['token'], user_id=user)
				user.set_password(request.data['password'])
				user.save()
				FrUser.forgot_pass_token = ''
				FrUser.save()
				return Response({'message': 'successfully updated new password'}, status=status.HTTP_200_OK)
			except Exception as e:
				print e
				return Response({'error': 'no token or user found'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			return Response({'error': 'no account found with this email'}, status=status.HTTP_400_BAD_REQUEST)




class update_device_token(viewsets.ViewSet):
	def create(self,request):
		try:
			user_id = request.data['user_id']
			device_token = request.data['device_token']
			user_obj = profile.objects.filter(id=int(request.data['user_id'])).update(device_token=request.data['device_token'])
			print ("user_obj",user_obj)
			return Response({'sucess': 'device token updated successfully'}, status=status.HTTP_200_OK)
		except Exception as e:
			return Response({'error': e.message}, status=status.HTTP_200_OK)


# class plan_viewset(viewsets.ModelViewSet):
# 	queryset = plans.objects.all()
# 	serializer_class = plan_serializer
#
# 	def create(self, request, *args, **kwargs):
# 		try:
# 			plan_ob = plans(plan_name = request.data['plan_name'])
# 			if 'plan_price' in request.data:
# 				plan_ob.plan_price = request.data['plan_price']
# 			if 'plan_description' in request.data:
# 				plan_ob.plan_description = request.data['plan_description']
#
#
# 			plan_ob.save()
# 			return response.Response({'success':'Plan Created Successfully'},status=status.HTTP_200_OK)
# 		except Exception as e:
# 			return response.Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)
#
# 	def update(self, request, *args, **kwargs):
# 		try:
# 			plan_ob = self.get_object()
# 			if 'plan_name' in request.data:
# 				plan_ob.plan_name = request.data['plan_name']
# 			if 'plan_price' in request.data:
# 				plan_ob.plan_price = request.data['plan_price']
# 			if 'plan_description' in request.data:
# 				plan_ob.plan_description = request.data['plan_description']
#
# 			plan_ob.save()
# 			return response.Response({'success': 'Plan Updated Successfully'}, status=status.HTTP_200_OK)
# 		except Exception,e:
# 			return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)



class work_level_viewset(viewsets.ModelViewSet):
	queryset = work_levels.objects.all()
	serializer_class = work_level_serializer
	def create(self, request, *args, **kwargs):
		try:
			level_ob = work_levels(level_name = request.data['level_name'])
			if 'level_description' in request.data:
				level_ob.level_description = request.data['level_description']
			level_ob.save()
			return response.Response({'success':'Level Created Successfully'},status=status.HTTP_200_OK)
		except Exception as e:
			return response.Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)

	def update(self, request, *args, **kwargs):
		try:
			level_ob = self.get_object()
			if 'level_name' in request.data:
				level_ob.level_name = request.data['level_name']
			if 'level_description' in request.data:
				level_ob.level_description = request.data['level_description']

			level_ob.save()
			return response.Response({'success': 'Level Updated Successfully'}, status=status.HTTP_200_OK)
		except Exception, e:
			return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

class membership_viewset(viewsets.ModelViewSet):
	queryset = membership.objects.all()
	serializer_class = membership_serializer

	def create(self, request, *args, **kwargs):
		try:
			mem_ob = membership(membership_name = request.data['membership_name'])
			if 'membership_description' in request.data:
				mem_ob.membership_description = request.data['membership_description']
			mem_ob.save()
			return response.Response({'success':'Membership Created Successfully'},status=status.HTTP_200_OK)
		except Exception as e:
			return response.Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)

	def update(self, request, *args, **kwargs):
		try:
			mem_ob = self.get_object()
			if 'membership_name' in request.data:
				mem_ob.membership_name = request.data['membership_name']
			if 'membership_description' in request.data:
				mem_ob.membership_description = request.data['membership_description']
			mem_ob.save()
			return response.Response({'success': 'Membership Updated Successfully'}, status=status.HTTP_200_OK)
		except Exception, e:
			return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)




class FacebookLoginViewset(ViewSet):
	def create(self,request):
		try:

			# allowed fields
			allowed_fields = ['id','email', 'name', 'first_name','last_name','picture','gender']

			# mandatory fields
			mandatory_fields = ['id']

			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(mandatory_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in mandatory_temp)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# checking whether user already exist or not
			if 'id' in request.data:

				# if user exist
				try:
					user_obj = User.objects.get(username=request.data['id'])
					if "first_name" in request.data:
						user_obj.first_name = request.data['first_name']
					if 'user_obj':
						findUser = profile.objects.get(user=user_obj.id)
						payload = jwt_payload_handler(user_obj)
						if api_settings.JWT_ALLOW_REFRESH:
							payload['orig_iat'] = timegm(
								datetime.utcnow().utctimetuple()
							)
						token =  jwt_encode_handler(payload)

						user_data = {}
						user_data['first_name'] = user_obj.first_name
						user_data['last_name'] = user_obj.last_name
						user_data['user_id'] = findUser.id
						user_data['cover_image'] = findUser.cover_image
						user_data['email'] = user_obj.email
						return Response({'token': token,
									 'user':user_data,
									 'id' : findUser.id,
									 'groups': user_obj.groups.all().values_list('name', flat=True),
									 'message': 'User successfully logged in'}, status=200)
				except Exception,e:
					print ("e",e)
					user_data = {}
					# if user doesn't exist
					try:
						user = User.objects.create_user(request.data['id'],request.data['id'])
						user.is_active = True
						try:
							if "email" in request.data:
								user.email = request.data['email']
							if "first_name" in request.data:
								user.first_name = request.data['first_name']
							if "last_name" in request.data:
								user.last_name = request.data['last_name']
							user.save()
							user_details = profile(user=user)
							if 'mobile' in request.data:
								user_details.mobile = request.data['mobile']
							if "gender" in request.data:
								user_details.gender = request.data['gender']
							if 'picture' in request.data:
								user_details.cover_image = request.data['picture']['data']['url']
							user_details.is_facebook_uesr = True
							user_details.save()

							c_group = Group.objects.get(name='User')
							user.groups.add(c_group)
							token = create_token(user.id)
							user_details.save()
							user_data = {}
							user_data['first_name'] = user.first_name
							user_data['last_name'] = user.last_name
							user_data['user_id'] = user_details.id
							user_data['cover_image'] = user_details.cover_image
							user_data['email'] = user.email
							return Response({'token': token,
										 'user':user_data,
										 'id' : user_details.id,
										 'groups': user.groups.all().values_list('name', flat=True),
										 'message': 'User successfully logged in'}, status=200)
						except Exception as e:
							print ("error e",e)
							return Response({'error': 'Email already exists'}, status=status.HTTP_400_BAD_REQUEST)
					except Exception as e:
						print("last except part",e)
						return Response({'error': 'It seems you already have an account !'}, status=status.HTTP_400_BAD_REQUEST)
					return Response({'token': token.key,'user':temp,'id':user_details.id,'message': 'user successfully logged in'}, status=status.HTTP_200_OK)
		except Exception as e:
			return Response({'error': 'unable to process...'}, status= status.HTTP_400_BAD_REQUEST)



class follow_viewset(viewsets.ModelViewSet):
	queryset = Follower.objects.all()
	serializer_class = followserializer

	def create(self, request, *args, **kwargs):
		follow_ob = Follower()
		follow_object = Follower.objects.filter(follower=int(request.data['follower']),
                                                     following=int(request.data['following']))
		if follow_object:
			return response.Response({"error": 'You are already following this user'},
									 status=status.HTTP_400_BAD_REQUEST)
		try:
			if 'follower' in request.data:
				follower_object = profile.objects.get(id=int(request.data['follower']))
				follow_ob.follower = follower_object

			if 'following' in request.data:
				following_object = profile.objects.get(id=int(request.data['following']))
				follow_ob.following = following_object
			follow_ob.save()

			return response.Response({"message": 'You are successfully following '}, status=status.HTTP_200_OK)
		except Exception as e:
			print (e)
			return response.Response({'Error': e.message})

class unfollow(ViewSet):
	def create(self, request):
		follow_object = Follower.objects.filter(follower=int(request.data['user']),
								 following=int(request.data['unfollowing']))
		if follow_object:
			follow_object.delete()
			return response.Response({"message":"you are unfollowing"})
		else:
			return response.Response("error")


class me_following(viewsets.ModelViewSet):
	queryset = Follower.objects.all()
	serializer_class = followserializer

	def list(self,request,*args,**kwargs):

		buddy_id = request.GET.get('userid')
		queryset = Follower.objects.filter(follower=int(buddy_id))
		serializer = followserializer(instance=queryset,many=True)
		qs = self.filter_queryset(queryset)
		page = self.paginate_queryset(qs)
		if page is not None:
			serializer = followserializer(page, many=True)
		return (self.get_paginated_response(serializer.data))


class my_followers(ViewSet):
	def list(self, request):
		buddy_id = request.GET.get('userid')
		queryset = Follower.objects.filter(following=int(buddy_id))
		serializer = followserializer(instance=queryset, many=True)
		return response.Response(serializer.data)




























