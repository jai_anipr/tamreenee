# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.


from .models import *
admin.site.register(profile)
admin.site.register(work_levels)

admin.site.register(membership)
